require 'test_helper'

class AbnormalDaysDetectorTest < ActiveSupport::TestCase
  def test_perform
    assert_equal %w(2014-09-09 2014-08-31 2014-08-22 2014-08-16), AbnormalDaysDetector.new.perform
  end
end
