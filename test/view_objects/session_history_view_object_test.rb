require 'test_helper'

class SessionHistoryViewObjectTest < ActiveSupport::TestCase
  def setup
    @session_histories = SessionHistory.all
    @session_history_view_object = SessionHistoryViewObject.new(@session_histories)
  end

  def test_success_failed_per_day
    data = @session_history_view_object.success_failed_per_day['2014-08-17']
    assert_equal 11, data[:failed]
    assert_equal 5, data[:success]
  end

  def test_duration_to_created_at
    data = @session_history_view_object.duration_to_created_at

    assert_equal 476, data.find { |i| i.first == '2014-08-19'}.last.to_i
  end

  def test_abnormal_failed_days
    assert_equal %w(2014-09-09 2014-08-31 2014-08-22 2014-08-16), @session_history_view_object.abnormal_failed_days
  end
end
