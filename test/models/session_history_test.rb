require 'test_helper'

class SessionHistoryTest < ActiveSupport::TestCase
  def test_failed
    SessionHistory.failed.each do |session_history|
      assert session_history.failed?
    end
  end
end
