class BarChart
  constructor: (selector, charData, columnNames, abnormalDays) ->
    @selector = selector
    @charData = charData
    @columnNames = columnNames
    @abnormalDays = abnormalDays
    @buildChart(charData)

  buildChart: (charData)->
    firstColumnName = @columnNames[0]
    secondColumnName = @columnNames[1]
    firstColumnData = [firstColumnName]
    secondColumnData = [secondColumnName]
    thirdColumnData = ['abnormal']
    columnsData = ['x']
    keys = Object.keys(charData).sort()
    self = @

    $.each(keys, (i, d)->
      date_info = charData[d]
      firstColumnData.push(date_info[firstColumnName])
      secondColumnData.push(date_info[secondColumnName])
      if self.abnormalDays.indexOf(d) != -1
        thirdColumnData.push(date_info[firstColumnName] + date_info[secondColumnName])
      else
        thirdColumnData.push(null)
      columnsData.push(d)
    )
    columns = [columnsData, firstColumnData, secondColumnData]
    chart = c3.generate
      bindto: @selector
      data:
        x: 'x'
        columns: columns
        type: 'bar'
        groups: [ @columnNames ]
      axis: x:
        type: 'category'
        categories: keys

    setTimeout ()->
      chart.load
        columns: [thirdColumnData]
      , 3000


class LineChart
  constructor: (selector, charData, columnNames) ->
    @selector = selector
    @charData = charData
    @columnNames = columnNames
    @buildChart(charData)

  buildChart: (charData)->
    firstColumnName = @columnNames[0]
    secondColumnName = @columnNames[1]
    xs = {}
    xs[firstColumnName] = secondColumnName

    firstColumnData = [firstColumnName]
    secondColumnData = [secondColumnName]
    $.each(charData, (i, d)->
      firstColumnData.push(d[0])
      secondColumnData.push(d[1])
    )

    c3.generate
      bindto: @selector
      data:
        x: 'x'
        columns: [firstColumnData, secondColumnData]
      axis: x:
        type: 'timeseries',
        tick: {
          format: '%Y-%m-%d'
        }

$ ->
  successFialedData = $('.success-failed-chart').data().successFailed
  abnormalDays = $('.abnormal-failed-chart').data().abnormalFailed
  new BarChart('.success-failed-chart', successFialedData, ['success', 'failed'], abnormalDays)

  durationToWorkerTimeData = $('.duration-to-worker-time-chart').data().durationWorkerTime
  new LineChart('.duration-to-worker-time-chart', durationToWorkerTimeData, ['x', 'duration'])

