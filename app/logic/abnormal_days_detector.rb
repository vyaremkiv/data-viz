class AbnormalDaysDetector
  def perform
    failed_session_histories_groped_by_day.keys.select do |date|
      count_failed = failed_session_histories_groped_by_day[date]
      count_failed && !available_range.include?(count_failed)
    end
  end

  private

  def available_range
    @_available_range ||= begin
      count = values.count
      q1 = 0.25 * (count - 1)
      q3 = 0.75 * (count - 1)

      quartile_1 = quartile(q1)
      quartile_3 = quartile(q3)

      iqr = quartile_3 - quartile_1

      (quartile_1 - iqr..quartile_3 + iqr)
    end
  end

  def quartile(q)
    values[q.floor] + (values[q.floor + 1]  - values[q.floor]) * (q - q.floor)
  end

  def values
    @_values ||= failed_session_histories_groped_by_day.values.uniq.sort
  end

  def failed_session_histories_groped_by_day
    @_failed_session_histories_groped_by_day ||= SessionHistory.failed.group_by do |sh|
      sh.created_date
    end.map { |k, v| [k, v.count] }.to_h
  end
end
