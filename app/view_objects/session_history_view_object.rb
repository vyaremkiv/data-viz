class SessionHistoryViewObject

  attr_reader :session_histories

  def initialize(session_histories)
    @session_histories = session_histories
  end

  def success_failed_per_day
    result = Hash.new()
    session_histories.each do |session_history|
      date_result = result[session_history.created_date] || { success: 0, failed: 0 }
      if session_history.failed?
        date_result[:failed] += 1
      else
        date_result[:success] += 1
      end
      result[session_history.created_date] = date_result
    end
    result
  end

  def duration_to_created_at
    session_histories_with_duration = session_histories.reject { |session_history| session_history.duration.zero? }

    result = Hash.new()
    session_histories_with_duration.each do |session_history|
      date_result = result[session_history.created_date] || []
      result[session_history.created_date] = date_result.push(session_history.duration)
    end
    result.map{ |date, durations| [date, durations.sum.to_f / durations.count]}
  end

  def abnormal_failed_days
    AbnormalDaysDetector.new.perform
  end
end
