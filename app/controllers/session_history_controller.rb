class SessionHistoryController < ApplicationController

  def index
    @session_histories = SessionHistory.all
    @session_history_view_object = SessionHistoryViewObject.new(@session_histories)
  end

end