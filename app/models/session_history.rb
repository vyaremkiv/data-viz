require 'csv'

class SessionHistory

  CSV_FILE_PATH = Rails.root.join('var', 'session_history.csv')

  FAILED_STATUES = %w(failed error)

  attr_reader :attributes, :created_at

  def initialize(attributes)
    @attributes = attributes
    @created_at = DateTime.parse(attributes['created_at'])
  end

  def created_date
    created_at.to_date.iso8601
  end

  def status
    attributes['summary_status']
  end

  def failed?
    FAILED_STATUES.include?(status)
  end

  def duration
    attributes['duration'].to_f
  end

  def self.failed
    all.select { |session_history| session_history.failed? }
  end

  def self.all
    @@_all ||= begin
      csv_text = File.read(CSV_FILE_PATH)

      csv = CSV.parse(csv_text, headers: true)
      csv.map { |row| SessionHistory.new(row.to_hash) }
    end
  end
end